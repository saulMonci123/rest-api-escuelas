import React from 'react';
import './App.css';
import App from './components/notifications';
import './styles/main.bundle.css';
import './styles/main.css';
import EmpresaProvider from "./context/EmpresaContext"
function Sistem() {

  return (
    <div className="App">
      <EmpresaProvider>
        <App/>
      </EmpresaProvider>
    </div>
  );
}

export default Sistem;
