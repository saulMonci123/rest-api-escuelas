import React, {createContext, useContext, useState} from 'react';

const EmpresaContext = createContext();
const EmpresaUpdateContext = createContext();


export function useEmpresaUpdateContext (){
    return useContext(EmpresaUpdateContext);
}


export function useEmpresaContext(){
    return useContext(EmpresaContext)
}


const EmpresaProvider = ({children}) => {
   const [empresa, setEmpresa] = useState(0)

   const toggleEmpresa = (emp)=>{
        setEmpresa(parseInt(emp));
   }

    return ( 
        <>
            <EmpresaContext.Provider  value={empresa}>
                <EmpresaUpdateContext.Provider value={toggleEmpresa}>
                    {children}
                </EmpresaUpdateContext.Provider>
            </EmpresaContext.Provider>
        </>    
    );
}
 
export default EmpresaProvider;