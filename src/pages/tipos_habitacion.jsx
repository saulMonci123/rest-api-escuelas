import React, { useRef, useState, useEffect } from 'react';
import Icon from "awesome-react-icons";
import { DashboardLayout } from '../components/layout';
import Table from '../components/table';
import { useFormik } from 'formik';
import CreateForm from '../components/createForm';
import UpdateForm from '../components/updateForm';
import swal from 'sweetalert';
import CrudView from '../components/crudView';
import SelectEmpresas from '../components/selectEmpresas';
import { useEmpresaContext } from '../context/EmpresaContext';
import { danger, success } from '../components/alerts';
import * as Yup from "yup";
//Se instancea el servicio donde se mandan a llamar los endpoints para este componente
import { habitacionesServices } from "../services/habitaciones/habitaciones.service";
const habSrv = new habitacionesServices;

const TiposHabitacion = () => {
    const columns = ["#", "Nombre", 'Descripcion' ,"Empresa"];
    const modal = useRef(null);
    const empresa = useEmpresaContext();
    const modalUpdate = useRef(null);
    const [inputs, setInputs] = useState([
        {
            type: "text",
            name: "nombre",
            label: "Nombre"
        },
        {
            type: "text",
            name: "descripcion",
            label: "Descripcion"
        }
    ])
    const [pager, setPager] = useState({});
    const [data, setData] = useState([]);


    const  validationSchema = Yup.object().shape({
        nombre: Yup.string().required('Este campo es obligatorio'),
        descripcion: Yup.string().required('Este campo es obligatorio'),
        empresa: Yup.string().required('Este campo es obligatorio'),
    });


    //Fomrulario crear
    const formik = useFormik({
        initialValues: {
            nombre: "",
            descripcion:"",
            empresa: empresa
        },
        onSubmit: values => {
            habSrv.createTipoHab(values).then((data) => {
                if(data){
                    loadPage(true);
                    modal.current.handleClose();
                    formik.resetForm();
                    success("Registro Exitoso", "Tipo de habitacion agregada correctamente")
                }
            });
        },
        validationSchema,
    });

    const clearFormik = ()=>{
        formik.setFieldValue('nombre', "");
        formik.setFieldValue('descripcion', "");
    }
  
    //Metodo para controlar la paginacion
    const loadPage = (recargar = false) => {
        const params = new URLSearchParams(window.location.search);
        const page = (!recargar) ? parseInt(params.get('page')) || 1 : 1;

        if (page !== pager.currentPage || recargar === true) {
            habSrv.getTipoHabitaciones(page, empresa).then((data) => {
                setData([...data.data]);
                setPager(data.pager);
            });
        }
    }

    useEffect(()=>{
		loadPage(true);
		formik.setFieldValue('empresa',empresa);
	},[empresa])

    //CallBack para eliminar tipo de habitacion
    const eliminarTipoHabitacion = (id) => {
        swal({
            title: "Estas seguro que quieres eliminar este tipo de habitacion?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((bool) => {
            if (bool) {
                habSrv.deleteTipoHabitacion(id).then(() => {
                    success("Registro borrado con exito", "Tipo de habitacion eliminada correctamente")
                    loadPage(true);
                });
            }
        })
    }



    return (
        <DashboardLayout>
            <CrudView
                title='Tipos Habitacion'
                buttons={
                    <button onClick={()=>{ clearFormik();  modal.current.handleShow()}} className="btn btn-primary btn-sm"><Icon name="plus" /></button>
                }
                table={
                    <Table
                        columns={columns}
                        loadPage={loadPage}
                        data={{ pager, data }}
                        actions={true}
                        eliminar={eliminarTipoHabitacion}
                        consultar={(id) => {
                            habSrv.getTipoHabitacion(id).then((data) => {
                                Object.keys(data).map((key) => {
                                    formik.setFieldValue(key,(typeof data[key] !== 'object') ? data[key] : data[key].id) ;
								});
                                modalUpdate.current.handleShow();
                            });
                        }}
                    />
                }

            />

            {/*Seccion de formularios */}
            <CreateForm
                inputs={inputs}
                formik={formik}
                ref={modal}
                title="Agregar Tipos de Habitacion"
                inputsExtra={
                    <SelectEmpresas
                        formik={formik}
                    />
                }
            >
            </CreateForm>

            <UpdateForm
                inputs={inputs}
                formik={formik}
                ref={modalUpdate}
                title="Actualizar"
                actualizar={(id) => {
                    habSrv.updateTipoHabitacion(id, formik.values).then(() => {
                        modalUpdate.current.handleClose();
                        formik.resetForm();
                        loadPage(true);
                        success("Actualizacion exitosa", "Tipo de habitacion actualizada correctamente");
                    });
                }}

                inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>
						</>
					)
				}
            />
        </DashboardLayout>
    );
}


export default TiposHabitacion;