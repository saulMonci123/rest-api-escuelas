import React, {useRef, useState, useEffect} from 'react';
import Icon from "awesome-react-icons";
import {DashboardLayout} from '../components/layout';
import Table from '../components/table';
import { useFormik } from 'formik';
import FrmBuscarAlumnoColegiatura from '../components/colegiaturas/formBuscarAlumnoColegiatura';
import { alumnsServices } from '../services/Colegiaturas/colegiaturas.service';
import CrudView from '../components/crudView';
import ModalDinamic from '../components/modalDinamic';
import Select from 'react-select'
import { conceptsServices } from '../services/Concepts/concepts.service';

//Se instancea el servicio donde se mandan a llamar los endpoints para este componente
const alumnSrv = new alumnsServices;
const conceptsSrv = new conceptsServices;
const Colegiaturas = ()=>{
    const columns = ["Matricula", "Nombre", "Escolaridad", "Grupo", "Grado", "Especialidad"];
    const [pager, setPager] = useState({});
	const [data, setData] = useState([]);
    const [alumno, setAlumno] = useState(null);
    const modal = useRef(null);
    const [table, setTable] = useState(false);
    const [optionsSelect , setOptionsSelect] = useState([]);
    const [pagosSelect, setPagodSelect] = useState([]);
    const [conceptosSeleccionados, setConceptosSeleccionados] = useState([]);
    const formik = useFormik({
        initialValues:{
            matricula:"",
            nombre: "",
            apellido_paterno: "",
            apellido_materno: ""
        },
        onSubmit: async (values) =>{
            const data = await alumnSrv.getAlumns(values);
            setData([...data]);
            setTable(true);
        }
    });


    const formikCantidad = useFormik({
        initialValues:{
            conceptos:[],
            tipoPago:0,
            alumno:null
        },
        onSubmit : (values)=>{
            console.log(values);
            conceptsSrv.realizarPago(values).then((data)=>{
                modal.current.handleClose();
                clearFormik2();
            }).catch(()=>{
                alert('Error');
            });

        }
    })
   
    const getConcetps = async () =>{
        const data = await conceptsSrv.getConcepts();
        const optionsToSelect = [];
        data.map((d)=>{
            optionsToSelect.push({
                value:d.id,
                label:d.descripcion
            });
        });
        setOptionsSelect([...optionsToSelect]);
    }


    const getTiposPagos = async ()=>{
        const data = await conceptsSrv.getTiposPagos();
        const optionsToSelect = [];
        data.map((d)=>{
            optionsToSelect.push({
                value:d.id,
                label:d.descripcion
            });
        });
        setPagodSelect([...optionsToSelect]);
    }


    //Metodo para controlar la paginacion
	const loadPage = (recargar = false) => {
		const params = new URLSearchParams(window.location.search);
		const page = (!recargar) ? parseInt(params.get('page')) || 1 : 1;

		if (page !== pager.currentPage || recargar) {
			// habSrv.getHabitaciones(page, empresa).then((data) => {
			// 	setData([...data.data]);
			// 	setPager(data.pager);
			// });
		}
	}
    

    const clearFormik = ()=>{
        formik.setFieldValue('matricula', "");
        formik.setFieldValue('nombre', "");
        formik.setFieldValue('apellido_paterno', "");
        formik.setFieldValue('apellido_materno', "");
    }

    const clearFormik2 = ()=>{
        formikCantidad.setFieldValue('conceptos', []);
        formikCantidad.setFieldValue('tipoPago', 0);
        formikCantidad.setFieldValue('alumno', null);
        setConceptosSeleccionados([]);
    }

   
    return (
        <DashboardLayout>
            {
              table === false && 
                <FrmBuscarAlumnoColegiatura
                    formik={formik}
                />
            }
            {
              table === true &&
              <>
              <CrudView
				title='Alumnos'
				buttons={
					<button onClick={()=>{ 
                        clearFormik();
                        setTable(false);
                    }} 
                    className="btn btn-primary btn-sm">
                        <Icon name="plus" />
                    </button>
				}
				table={
					<Table
						columns={columns}
						loadPage={loadPage}
						data={{ pager, data }}
						actions={true}
                        buttons='cobrar'
                        cobrarMethod={(id)=>{
                            formikCantidad.values.alumno = id;
                            getConcetps();
                            getTiposPagos();
                            modal.current.handleShow();

                        }}
					>
                        
                    </Table>
				}
			  />  
                <ModalDinamic
                    ref={modal}
                    title="Cobrar"
                >
                    <div className="row">
                        <div className="col-xl-12">
                            <Select 
                                options={optionsSelect}
                                placeholder="Seleccione un concepto"
                                isMulti
                                name="concepto"
                                onChange={(data)=>{
                                    console.log('Es es el evento del select',data);
                                    if(data){
                                        const arrayData = [];
                                        const arrayObjectData = [];
                                        data.map((d)=>{
                                            arrayData.push({
                                                id:d.value,
                                                costo:0
                                            });
                                            arrayObjectData.push(d);
                                        })
                                        setConceptosSeleccionados(arrayObjectData);
                                        formikCantidad.values.conceptos =  arrayData;
                                    }else{
                                        formikCantidad.values.conceptos =  [];
                                        setConceptosSeleccionados(null);
                                    }
                                }}
                            />   
                        </div>
                    </div>
                    <div className="row form-group" style={{marginTop:"9px"}}>
                        <div className="col-xl-12">
                            {
                                !!conceptosSeleccionados && conceptosSeleccionados.map((d,i)=>{
                                    return (
                                    <div className="row" style={{marginTop:"9px"}} key={i}>
                                        <div className="col-xl-12">
                                            <label htmlFor="">{d.label}</label>
                                            < input 
                                                className="form-control form-control-sm" 
                                                type="number" 
                                                name="cantidad" 
                                                onChange={(value)=>{
                                                    formikCantidad.values.conceptos.map((c, i)=>{
                                                        if(c.id === d.value){
                                                            formikCantidad.values.conceptos[i].costo =  value.target.value;
                                                            console.log(formikCantidad.values.conceptos[i])
                                                        }
                                                    })
                                                }}
                                                placeholder="Cantidad a cobrar"
                                            />
                                        </div>
                                    </div>
                                )})
                            }
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-xl-12">
                            <Select 
                                options={pagosSelect}
                                placeholder="Tipo de pago"
                                name="tipoPago"
                                onChange={(data)=>{
                                    formikCantidad.values.tipoPago = data.value;
                                }}
                            />   
                        </div>
                    </div>
                    <div className="row form-group">
                        <div className="col-xl-12">
                            <button className="btn btn-primary btn-block" onClick={()=>{
                                formikCantidad.submitForm();
                            }}>Aceptar</button>
                        </div>
                    </div>
                </ModalDinamic>
              </>
            }
            
        </DashboardLayout>

        
    );

    
}


export default Colegiaturas;
