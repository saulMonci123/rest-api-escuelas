import React, { useRef, useState, useEffect } from 'react';
import Icon from "awesome-react-icons";
import { DashboardLayout } from '../components/layout';
import Table from '../components/table';
import { useFormik } from 'formik';
import CreateForm from '../components/createForm';
import UpdateForm from '../components/updateForm';
import swal from 'sweetalert';
import * as Yup from "yup";
import CrudView from '../components/crudView';
import SelectEmpresas from '../components/selectEmpresas';
import SelectTiposHabitacion from '../components/selectTipoHabitaciones';
import { useEmpresaContext } from '../context/EmpresaContext';
import { success } from '../components/alerts';



//Se instancea el servicio donde se mandan a llamar los endpoints para este componente
import { habitacionesServices } from "../services/habitaciones/habitaciones.service";
const habSrv = new habitacionesServices;


const Habitaciones = () => {
	const columns = ["#", "Numero Habitacion", "Tipo", "Empresa"];
	const modal = useRef(null);
	const modalUpdate = useRef(null);
	const [pager, setPager] = useState({});
	const [data, setData] = useState([]);
    const empresa = useEmpresaContext();
	const [inputs, setInputs] = useState([{
		type: "text",
		name: "nombre",
		label: "Numero"
	}]);

	const [empresaId, setEmpresaId] = useState(null);

	
    const validationSchema = Yup.object().shape({
        nombre: Yup.string().required('Este campo es obligatorio'),
        tipo: Yup.string().required('Este campo es obligatorio'),
	});
	
	//Fomrulario crear y actualizar
	const formik = useFormik({
		initialValues: {
			nombre: "",
			empresa: empresa ,
			tipo: "",
		},
		onSubmit: (values) => {
			habSrv.create(values).then(() => {
				loadPage(true);
				modal.current.handleClose();
				formik.resetForm();
				success("Registro Exitoso", "Habitacion agregada correctamente")
			});
		},
		validationSchema
	});

	const clearFormik = ()=>{
        formik.setFieldValue('nombre', "");
        formik.setFieldValue('tipo', "");
    }


	//Metodo para controlar la paginacion
	const loadPage = (recargar = false) => {
		const params = new URLSearchParams(window.location.search);
		const page = (!recargar) ? parseInt(params.get('page')) || 1 : 1;

		if (page !== pager.currentPage || recargar) {
			habSrv.getHabitaciones(page, empresa).then((data) => {
				setData([...data.data]);
				setPager(data.pager);
			});
		}
	}

	useEffect(()=>{
		loadPage(true);
		formik.setFieldValue('empresa',empresa);
		setEmpresaId(empresa);
	},[empresa])

	//CallBack para eliminar tipo de habitacion
	const eliminarTipoHabitacion = (id) => {
		swal({
			title: "Estas seguro que quieres eliminar esta habitacion?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((bool) => {
			if (bool) {
				habSrv.delete(id).then(() => {
					success("Registro borrado con exito", "Habitacion eliminada correctamente")
					loadPage(true);
				});
			}
		})
	}



	return (
		<DashboardLayout>
			<CrudView
				title='Habitaciones'
				buttons={
					<button onClick={()=>{ clearFormik();  modal.current.handleShow()}} className="btn btn-primary btn-sm"><Icon name="plus" /></button>
				}
				table={
					<Table
						columns={columns}
						loadPage={loadPage}
						data={{ pager, data }}
						actions={true}
						eliminar={eliminarTipoHabitacion}
						consultar={(id) => {
							habSrv.get(id).then((data) => {
								console.log(data);
								Object.keys(data).map((key) => {
									formik.setFieldValue(key,(typeof data[key] !== 'object') ? data[key] : data[key].id) ;
								});
								console.log(formik.values);
								modalUpdate.current.handleShow();
							});
						}}
					/>
				}
			/>

			{/*Seccion de formularios */}
			<CreateForm
				inputs={inputs}
				formik={formik}
				ref={modal}
				title="Agregar Habitacion"
				inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>

							<SelectTiposHabitacion
								formik={formik}
								empresa={empresaId}
							/>
						</>
					)
				}
			/>

			<UpdateForm
				inputs={inputs}
				formik={formik}
				ref={modalUpdate}
				title="Actualizar"
				actualizar={(id) => {
					habSrv.update(id, formik.values).then(() => {
						modalUpdate.current.handleClose();
						formik.resetForm();
						loadPage(true);
						success("Actualizacion exitosa", "Habitacion actualizada correctamente");
					});
				}}
				inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>

							<SelectTiposHabitacion
								formik={formik}
								empresa={empresaId}
							/>
						</>
					)
				}
			/>
		</DashboardLayout>
	);
}


export default Habitaciones;
