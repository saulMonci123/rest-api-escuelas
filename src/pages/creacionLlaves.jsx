import React, {useRef, useState, useEffect} from 'react';
import {DashboardLayout} from '../components/layout';
import ContainerForm from '../components/containerForm';
import { useFormik } from 'formik';
import FormCreacionLlaves from '../components/formCreacionLlaves';
import { useEmpresaContext } from '../context/EmpresaContext';
import visionServices from "../services/vision/vision.service"
const visionSrv = new visionServices;


const CreacionLlaves = ()=>{
    const empresa = useEmpresaContext();
    const formik = useFormik({
        initialValues:{
            empresa:empresa,
            nombre:"",
            habitaciones:[],
            areas:[],
            cantidad:0,
            habitacion_principal:"",
            fecha_llegada:"",
            fecha_salida:"",
            hora_entrada:"",
            hora_salida:"",
            socketId:""
        },
        onSubmit: values =>{
            console.log(values);
            visionSrv.createCheckIn(values).then((data)=>{
                console.log(data);
            });

        }
    })

    useEffect(()=>{
        formik.setFieldValue('empresa', empresa);
    },[empresa])

    
    return (
        <DashboardLayout>
            <ContainerForm 
                titulo="Creacion de llaves"
            >
                <FormCreacionLlaves
                    formik={formik}
                />
            </ContainerForm>
        </DashboardLayout>
    );
}


export default CreacionLlaves;
