import React, { useEffect } from "react";
import { BrowserRouter, Switch, Route, useHistory, Redirect } from "react-router-dom";

//Pages
import Colegiaturas from "./colegiaturas";
import Estaciones from "./estaciones";
import Habitciones from "./habitaciones";
import TiposHabitacion from "./tipos_habitacion";
import AreasComunes from "./areas_comunes";
import Home from "./home";
import Login from "./login";
import Test from "./test";
import CreacionLlaves from "./creacionLlaves";

import {AuthHelperService} from "../services/Auth/auth.service";

const MiddlewareRoute = (props)=>{
  const authSrv = new AuthHelperService;
  const token = authSrv.getToken();
  return (token) ? <Route {...props} /> : <Redirect to="/login"/>
}



const Routes = () => {
  return (
    <BrowserRouter >
      <Switch>
        <Route exact path="/login" component={Login}/>
        <MiddlewareRoute exact path="/" component={Home}/>
        <MiddlewareRoute exact path="/colegiaturas" component={Colegiaturas}/>
        <MiddlewareRoute exact path="/estaciones" component={Estaciones}/>
        <MiddlewareRoute exact path="/habitaciones" component={Habitciones}/>
        <MiddlewareRoute exact path="/tipos_habitacion" component={TiposHabitacion}/>
        <MiddlewareRoute exact path="/areas_comunes" component={AreasComunes}/>
        <MiddlewareRoute exact path="/herramientas/tester" component={Test}/>
        <MiddlewareRoute exact path="/herramientas/creacion/llaves" component={CreacionLlaves}/>
      </Switch>
    </BrowserRouter>
  );
};

export default Routes;
