import React, { useRef, useState, useEffect } from 'react';
import Icon from "awesome-react-icons";
import { DashboardLayout } from '../components/layout';
import Table from '../components/table';
import { useFormik } from 'formik';
import CreateForm from '../components/createForm';
import UpdateForm from '../components/updateForm';
import swal from 'sweetalert';
import * as Yup from "yup";
import CrudView from '../components/crudView';
import SelectEmpresas from '../components/selectEmpresas';
import { useEmpresaContext } from '../context/EmpresaContext';


//Se instancea el servicio donde se mandan a llamar los endpoints para este componente
import { AreasComunesService } from "../services/areasComunes/areasComunes.service";
import { success } from '../components/alerts';
const areasComunesSrv = new AreasComunesService;


const Habitaciones = () => {
	const columns = ["#", "Area Comun", "Empresa"];
	const modal = useRef(null);
	const modalUpdate = useRef(null);
	const empresa = useEmpresaContext();
	const [pager, setPager] = useState({});
	const [data, setData] = useState([]);
	const [inputs, setInputs] = useState([{
		type: "text",
		name: "nombre",
		label: "Area"
	}]);

	const  validationSchema = Yup.object().shape({
        nombre: Yup.string().required('Este campo es obligatorio'),
        empresa: Yup.string().required('Este campo es obligatorio'),
    });

	//Fomrulario crear y actualizar
	const formik = useFormik({
		initialValues: {
			nombre: "",
			empresa: empresa,
		},
		onSubmit: values => {
			areasComunesSrv.create(values).then((data) => {
				if(data){
					loadPage(true);
					modal.current.handleClose();
					formik.resetForm();
					success("Registro Exitoso", "Area agregada correctamente")
				}
			});
		},
		validationSchema
	});

	const clearFormik = ()=>{
        formik.setFieldValue('nombre', "");
     }



	//Metodo para controlar la paginacion
	const loadPage = (recargar = false) => {
		const params = new URLSearchParams(window.location.search);
		const page = (!recargar) ? parseInt(params.get('page')) || 1 : 1;

		if (page !== pager.currentPage || recargar) {
			areasComunesSrv.getAreasComunes(page, empresa).then((data) => {
				setData([...data.data]);
				setPager(data.pager);
			});
		}
	}


	useEffect(()=>{
		loadPage(true);
		formik.setFieldValue('empresa',empresa);
	},[empresa])

	//CallBack para eliminar tipo de habitacion
	const eliminarTipoHabitacion = (id) => {
		swal({
			title: "Estas seguro que quieres eliminar esta area?",
			icon: "warning",
			buttons: true,
			dangerMode: true,
		}).then((bool) => {
			if (bool) {
				areasComunesSrv.delete(id).then(() => {
					success("Registro borrado con exito", "Habitacion eliminada correctamente")
					loadPage(true);
				});
			}
		})
	}



	return (
		<DashboardLayout>
			<CrudView
				title='Areas Comunes'
				buttons={
					<button onClick={()=>{ clearFormik();  modal.current.handleShow()}} className="btn btn-primary btn-sm"><Icon name="plus" /></button>
				}
				table={
					<Table
						columns={columns}
						loadPage={loadPage}
						data={{ pager, data }}
						actions={true}
						eliminar={eliminarTipoHabitacion}
						consultar={(id) => {
							areasComunesSrv.get(id).then((data) => {
								Object.keys(data).map((key) => {
									formik.setFieldValue(key,(typeof data[key] !== 'object') ? data[key] : data[key].id) ;
								});
								modalUpdate.current.handleShow();
							});
						}}
					/>
				}

			/>

			{/*Seccion de formularios */}
			<CreateForm
				inputs={inputs}
				formik={formik}
				ref={modal}
				title="Agregar Area"
				inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>
						</>
					)
				}
			/>

			<UpdateForm
				inputs={inputs}
				formik={formik}
				ref={modalUpdate}
				title="Actualizar"
				actualizar={(id) => {
					areasComunesSrv.update(id, formik.values).then(() => {
						modalUpdate.current.handleClose();
						formik.resetForm();
						loadPage(true);
						success("Actualizacion exitosa", "Area actualizada correctamente");
					});
				}}
				inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>
						</>
					)
				}
			/>
		</DashboardLayout>
	);
}


export default Habitaciones;
