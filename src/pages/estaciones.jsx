import React, { useRef, useState, useEffect } from 'react';
import Icon from "awesome-react-icons";
import { DashboardLayout } from '../components/layout';
import Table from '../components/table';
import { useFormik } from 'formik';
import CreateForm from '../components/createForm';
import UpdateForm from '../components/updateForm';
import swal from 'sweetalert';
import * as Yup from "yup";

//Se instancea el servicio donde se mandan a llamar los endpoints para este componente
import { estacionesServices } from "../services/Estaciones/estaciones.service";
import CrudView from '../components/crudView';
import SelectEmpresas from '../components/selectEmpresas';
import { useEmpresaContext } from '../context/EmpresaContext';
const estSrv = new estacionesServices;


const Estaciones = () => {
    const columns = ["#", "Nombre", "Estatus" ,"Empresa"];
    const modal = useRef(null);
    const modalUpdate = useRef(null);
    const empresa = useEmpresaContext();
    const inputs = [
        {
            type: "text",
            name: "nombre",
            label: "Nombre"
        },
    ];


    const validationSchema = Yup.object().shape({
        nombre: Yup.string().required('Este campo es obligatorio'),
        empresa: Yup.string().required('Este campo es obligatorio'),
    });


    //Fomrulario crear y actualizar
    const formik = useFormik({
        initialValues: {
            nombre: "",
            empresa:empresa
        },
        onSubmit: values => {
            estSrv.create(values).then(() => {
                loadPage(true);
                modal.current.handleClose();
                formik.resetForm();
                swal("Estacion agregada correctamente", {
                    icon: "success",
                });
            });
        },
        validationSchema
    });


    const clearFormik = ()=>{
        formik.setFieldValue('nombre', "");
    }

    const [pager, setPager] = useState({});
    const [data, setData] = useState([]);

    //Metodo para controlar la paginacion
    const loadPage = (recargar = false) => {
        const params = new URLSearchParams(window.location.search);
        const page = (!recargar) ? parseInt(params.get('page')) || 1 : 1;

        if (page !== pager.currentPage || recargar) {
            estSrv.getEstaciones(page, empresa).then((data) => {
                if (data.status === 400) { setData([]); setPager([]); return; }
                setData([...data.data]);
                setPager(data.pager);
            });
        }
    }

    useEffect(()=>{
        loadPage(true);
        formik.setFieldValue('empresa',empresa);
	},[empresa])


    //CallBack para eliminar tipo de habitacion
    const eliminarTipoHabitacion = (id) => {
        swal({
            title: "Estas seguro que quieres eliminar esta estacion?",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((bool) => {
            if (bool) {
                estSrv.delete(id).then(() => {
                    swal("Estacion eliminada correctamente", {
                        icon: "success",
                    });
                    loadPage(true);
                });
            }
        })
    }


    return (
        <DashboardLayout>
            <CrudView
				title='Estaciones'
				buttons={
					<button onClick={ ()=>{clearFormik(); modal.current.handleShow();}} className="btn btn-primary btn-sm"><Icon name="plus"/></button>
				}
				table={
                    <Table
                        columns={columns}
                        loadPage={loadPage}
                        data={{ pager, data }}
                        actions={true}
                        eliminar={eliminarTipoHabitacion}
                        consultar={(id) => {
                            estSrv.get(id).then((data) => {
                                Object.keys(data).map((key) => {
                                    formik.setFieldValue(key, (typeof data[key] !== 'object') ? data[key] : data[key].id);
                                });
                                modalUpdate.current.handleShow();
                            });
                        }}
                    />
				}
			/>

            {/*Seccion de formularios */}
            <CreateForm
                inputs={inputs}
                formik={formik}
                ref={modal}
                title="Agregar Estacion"
                inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>
						</>
					)
				}
            />

            <UpdateForm
                inputs={inputs}
                formik={formik}
                ref={modalUpdate}
                title="Actualizar"
                actualizar={(id) => {
                    estSrv.update(id, formik.values).then(() => {
                        modalUpdate.current.handleClose();
                        formik.resetForm(); 
                        loadPage(true);
                        swal("Estacion actualizada correctamente", {
                            icon: "success",
                        });
                    });
                }}
                inputsExtra={
					(
						<>
							<SelectEmpresas
								formik={formik}
							/>
						</>
					)
				}
            />
        </DashboardLayout>
    );
}


export default Estaciones;
