import React, { useEffect, useState } from 'react';
import {DashboardLayout} from '../components/layout';
import {TestingServices} from "../services/testing/testing.service"
import { useFormik } from 'formik';
const testSrv = new TestingServices;

const Test = () => {
    const [clients, setClients] = useState([]);
    const formik = useFormik({
        initialValues:{
            socketId:"",
            id: null,
            includeDeleted: false,
            includeInternal: true
        },
        onSubmit: values =>{
            testSrv.dispararAccion(values).then((data)=>{
                alert(data.message);
            });
        }
    });
    
    useEffect(()=>{
        loadClients();
    },[]);

    const loadClients = ()=>{
        testSrv.getClients().then((data)=>{
            console.log('Estos son los clientes que se reciben',data);
            setClients([...data])
        });
    }



    return  (
        <DashboardLayout>
            <div className="container d-flex justify-content-center">
                <div className="card" style={{marginTop:"10%", width:"40%"}}>
                    <div class="card-header text-left">Testing clientes conectados al socket</div>
                    <div className="card-body">  
                        <form>
                            <div className="form-group">
                                <select name="socketId" value={formik.values['socketId']}  onChange={formik.handleChange} className="form-control form-control-sm">
                                    <option value="">Seleccion un cliente</option>
                                    {
                                    !!clients && clients.map((d)=> <option value={d.connectionId}>{d.nombre}</option>)  
                                    }
                                </select>
                            </div>
                            <div className="form-group">
                                <button type="button" onClick={formik.handleSubmit} className="btn btn-primary btn-block">Disparar accion</button>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </DashboardLayout> 
        )
}


export default Test;