import React, {useEffect} from 'react';
import "../styles/login.css"
import { Redirect, useHistory  } from "react-router-dom";
import {AuthHelperService} from "../services/Auth/auth.service";
import { useFormik } from 'formik';
import { warning, info, danger } from "../components/alerts";
import * as Yup from "yup";
const authSrv = new AuthHelperService;


const Login = () => {

   const  validationSchema = Yup.object().shape({
       email: Yup.string().email('Es necesario un correo').required('Este campo es obligatorio'),
       password: Yup.string().required('Este campo es obligatorio')
   });

  const history = useHistory();
  const formik = useFormik({
      initialValues:{
          email:"",
          password:""
      },
      onSubmit: (values)=>{ 
        authSrv.login(values).then((data)=>{
            const token = data.data.token;
            const user = JSON.stringify(data.data.user);
            authSrv.setToken(token);
            authSrv.setUser(user);
            history.push('/');
        }).catch((error)=>{
            danger('Error en inicio de sesion' , 'Error en el servidor');
        });
      },
      validationSchema,
  });

  useEffect(()=>{
    const params = new URLSearchParams(window.location.search);
    const expire = params.get('expire') || false;
    if(expire){
        info('Expiro Sesion','La sesion del usuario expiro por inactividad');
    }
  },[])

  const token = authSrv.getToken();

  return (token) ? (
            <Redirect to="/"/>
        ) : (
                <div className="container d-flex justify-content-center">
                    <div className="card" style={{marginTop:"5em", width:"30%"}}>
                        <div class="card-header text-left">Iniciar Sesion</div>
                        <div className="card-body">
                            <form className=" text-left">
                                <div className="form-group">
                                    <label for="user">Usuario</label>
                                    <input name="email" type="email" className="form-control" id="user" required aria-describedby="emailHelp" onChange={formik.handleChange}></input>
                                </div>
                                <div className="form-group">
                                    <label for="pas">Contraseña</label>
                                    <input name="password" type="password" className="form-control" id="pass" required onChange={formik.handleChange}></input>
                                </div>
                                <button type="button" onClick={formik.handleSubmit} className="btn btn-primary btn-block">Iniciar Sesion</button>
                            </form>
                        </div>
                    </div>
                </div>
            )
}


export default Login;