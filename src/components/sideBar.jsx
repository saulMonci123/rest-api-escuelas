import { Navigation } from "react-minimal-side-navigation";
import { useHistory, useLocation, Link } from "react-router-dom";
import Icon from "awesome-react-icons";
import React, { useState, useEffect } from "react";
import {AuthHelperService} from "../services/Auth/auth.service";
import "react-minimal-side-navigation/lib/ReactMinimalSideNavigation.css";


const authSrv = new AuthHelperService;

export function SideBar() {
  const history = useHistory();
  const location = useLocation();
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);
  // const inactivityTime = function () { 
  //   var t; window.onload = resetTimer; 
  //   // DOM Events 
  //   document.onmousemove = resetTimer; document.onkeypress = resetTimer; 
  //   function logout() { 
  //     authSrv.removeToken();
  //     authSrv.removeUser();
  //     history.push('/login?expire=true')
  //   } 
  //   function resetTimer() { 
  //     clearTimeout(t); 
  //     t = setTimeout(logout, 86400) // 1000 milisec = 1 sec  //86400 = 1 day
  //   } 
  // };
  // inactivityTime();
  return (
    <React.Fragment>

      <div>
        <button
          className="btn-menu"
          onClick={() => setIsSidebarOpen(true)}
          type="button"
        >
          <Icon name="burger" className="w-6 h-6" />
        </button>
      </div>

      {/* Sidebar */}
      <div
        className={`fixed inset-y-0 left-0 z-30 w-64 overflow-y-auto transition duration-300 ease-out transform translate-x-0 bg-white border-r-2 lg:translate-x-0 lg:static lg:inset-0 ${
          isSidebarOpen ? "ease-out translate-x-0" : "ease-in -translate-x-full"
        }`}
      >

        <Navigation
          activeItemId={location.pathname}
          onSelect={({ itemId }) => {
            history.push(itemId);
          }}
          items={[
            {
              title: "Colegiaturas",
              itemId: "/colegiaturas",
              elemBefore: () => <Icon name="burger" />
            }
          ]}
        />
        <Navigation
          activeItemId={location.pathname}
          items={[
            {
              title: "Cerrar Sesion",
              itemId: "/",
              elemBefore: () => <Icon name="arrow-left" />
            }
          ]}
          onSelect={({ itemId }) => {
            authSrv.removeToken();  
            authSrv.removeUser();  
            history.push(itemId);
          }}
        />
      </div>

      
      
    </React.Fragment>
  );
}
