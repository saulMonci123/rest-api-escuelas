import React from "react";
import Header from "./header";
import { SideBar } from "./sideBar";
import BodyWrapper from "./bodyWraper";

export const DashboardLayout = ({ children }) => {
  return (
    <BodyWrapper>
      <Header/>
      <div className="flex h-screen bg-gray-200" style={{height:"55em"}}>
        <SideBar />

        <div className="flex flex-col flex-1 overflow-hidden">
          <main className="content">
            <section className="sm:flex-row flex flex-col flex-1">
              <div
                className="content-box"
                style={{ flexGrow: 2, flexBasis: "0%" }}
              >
                {children}
              </div>
            </section>
          </main>
        </div>
      </div>
    </BodyWrapper>
  );
};
