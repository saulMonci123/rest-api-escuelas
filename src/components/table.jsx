import React from 'react';
import { Link } from "react-router-dom";
import Icon from "awesome-react-icons";

class Table extends React.Component {
 
  constructor(props){
    super(props);
    this.state = {
      pager:this.props.data.pager
    }
  }

  componentDidMount() {
      this.props.loadPage();
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.data.data.length > 0){
      this.props.loadPage();
    }
  }



  render() {
    const pager = this.props.data.pager;
    const data = this.props.data.data;
    const children = this.props.children
    const actions = this.props.actions || false;
    const buttons = this.props.buttons || '';
    console.log(data);
    if(data.length > 0){
      return (
        <>
          <table className="table table-sm border" >
            <thead>
              <tr>
               {
                   !!this.props.columns && this.props.columns.map(column => ( <th scope="col">{column}</th> )) 
               }
               {
                 (actions) ? <th scope="col">Acciones</th> : ""
               }
              </tr>
            </thead>
            <tbody>
              {
                !!data && data.map((d)=>{
                  return(
                    <tr key={d.id}>
                        {Object.keys(d).map((key) => {
                          if(key !== 'id'){
                            return (<th>{d[key]}</th>)
                          }
                        })}
                        {(actions) ? (
                          <th scope="col">
                            {
                            buttons === '' &&
                              <>
                                <button onClick={()=>{this.props.eliminar(d.id)}} className="btn btn-danger btn-sm"><Icon name="arrow-down" title="Eliminar"/></button>
                                <button onClick={()=>{this.props.consultar(d.id)}} className="btn btn-success btn-sm ml-1"><Icon name="help-circle" title="Actualizar"/></button>
                              </>
                            }
                            {
                              buttons === "cobrar" && 
                                <>
                                  <button 
                                      className="btn btn-success btn-sm"
                                      onClick={()=>{this.props.cobrarMethod(d.id)}}
                                  >
                                      Cobrar
                                  </button>
                                </>
                            }
                          </th>
                        ) : ""}
                    </tr>
                  )
                })
              }
            </tbody>
          </table>
          <div className="row">
            <div className="col"></div>
            <div className="col">
              { !!pager && !!pager.pages && pager.pages.length &&
                  <ul className="pagination">
                      <li className={`page-item first-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                          <Link to={{ search: `?page=1` }} className="page-link">Primero</Link>
                      </li>
                      <li className={`page-item previous-item ${pager.currentPage === 1 ? 'disabled' : ''}`}>
                          <Link to={{ search: `?page=${pager.currentPage - 1}` }}  className="page-link">Anterior</Link>
                      </li>
                      {pager.pages.map(page =>
                          <li key={page} className={`page-item number-item ${pager.currentPage === page ? 'active' : ''}`}>
                              <Link to={{ search: `?page=${page}` }}  className="page-link">{page}</Link>
                          </li>
                      )}
                      <li className={`page-item next-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                          <Link to={{ search: `?page=${pager.currentPage + 1}` }}  className="page-link">Siguiente</Link>
                      </li>
                      <li className={`page-item last-item ${pager.currentPage === pager.totalPages ? 'disabled' : ''}`}>
                          <Link to={{ search: `?page=${pager.totalPages}` }}  className="page-link">Ultimo</Link>
                      </li>
                  </ul>
              }
            </div>
            <div className="col"></div>
          </div>
        </>
      )
    }else{
      return "No se encontraron registros"
    }
   
  }
}



export default Table;


