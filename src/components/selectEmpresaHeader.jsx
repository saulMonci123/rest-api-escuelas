import React, { Component, useEffect, useState } from 'react';
import { empresasServices } from "../services/Empresas/empresas.service";
import {useEmpresaUpdateContext, useEmpresaContext} from "../context/EmpresaContext";


//Se instancea el servicio de empresas
const empresasSrv = new empresasServices;

function SelectEmpresaHeader() {
   const empresa = useEmpresaContext();
   const [data, setData] = useState([]);
   const toggleEmpresa = useEmpresaUpdateContext();

   

   const getEmpresas = async () => {
      //const data = await empresasSrv.getJson();
      //setData([...data]);
   }

   useEffect(()=>{
    getEmpresas();
   },[])

   const handleOnChange = (e)=>{
      toggleEmpresa(e.target.value)
   }

    return (
        <>
        <div class="input-group-prepend">
            <label class="input-group-text" for="inputGroupSelect01">Empresa</label>
        </div>
        <select class="custom-select" id="inputGroupSelect01" onChange={handleOnChange}>
            <option disabled value="">Eliga una empresa</option>
            {
                !!data && data.map((d, i) => {
                    if(empresa === 0  && i === 0){
                        toggleEmpresa(d.id)
                    }
                    return (
                        <option id={d.id} selected={(d.id === empresa || i === 0) ? true : false} value={d.id}  >{d.nombre}</option>
                    )
                })
            }
        </select>
        </>
    );
}

export default SelectEmpresaHeader;