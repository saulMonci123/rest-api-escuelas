import React, {useEffect, useState} from 'react';
import {habitacionesServices} from "../services/habitaciones/habitaciones.service";

const habitacionesSrv = new habitacionesServices;

const SelectTiposHabitacion = (props) => {
    const [tiposHabitacion, setTiposHabitacion] = useState([])

    useEffect(()=>{
        obtenerTiposHabitaciones();
    }, [props.empresa])

    const obtenerTiposHabitaciones = async () => {
        if(props.empresa !== null){
            const json = await habitacionesSrv.getTiposHabitacionByEmpresa(props.empresa);
            setTiposHabitacion([...json]);
        }
    }

    return (
        <>
            <label>Seleccione un tipo de habitacion</label>
            <select  
                className="form-control form-control-sm"
                onChange={props.formik.handleChange}
                value={props.formik.values['tipo']}
                onChange={props.formik.handleChange}
                name="tipo"
            >
                <option disabled value="">Seleccione un tipo de habitacion</option>
                {
                    !!tiposHabitacion &&  tiposHabitacion.map((d)=>{
                        return (
                            <option id={d.id} value={d.id}  >{d.nombre}</option>
                        )
                    })
                }
            </select>
        </>
    );

}


export default SelectTiposHabitacion;