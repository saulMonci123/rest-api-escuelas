import React, { useEffect, useState } from 'react';
import {TestingServices} from "../services/testing/testing.service"
const testSrv = new TestingServices;

const ClientsSelect = ({formik}) => {
    const [clients, setClients] = useState([]);
    
    useEffect(()=>{
        loadClients();
    },[]);

    const loadClients = ()=>{
        testSrv.getClients().then((data)=>{
            setClients([...data])
        });
    }

    return  (
        <>
            <div className="form-group">
                <select name="socketId"  onChange={formik.handleChange} className="form-control form-control-sm">
                    <option value="">Seleccion un cliente</option>
                    {
                        !!clients && clients.map((d)=> <option value={d.connectionId}>{d.nombre}</option>)  
                    }
                </select>
            </div>
        </>    
    )
}


export default ClientsSelect;