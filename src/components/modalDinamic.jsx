import React, { useState } from 'react';
import { Modal, Button, Form } from 'react-bootstrap';



class ModalDinamic extends React.Component{

    constructor(props){
        super(props);
        this.state={
            show:false
        }
    }

    handleClose = () => this.setState({show:false});
    handleShow = () => this.setState({show:true});

    render() {
        const {title, children} = this.props;
        return (
            <Modal show={this.state.show}  onHide={this.handleClose}>
                <Modal.Header closeButton>
                <Modal.Title>{title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                {
                    children
                }
                </Modal.Body>
            </Modal>
        )
    }
}

export default ModalDinamic;