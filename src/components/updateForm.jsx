import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap';




/**
 * Componente para actualizar datos
 */
class UpdateForm extends React.Component {
 
  constructor(props){
    super(props);
    this.state={
        show:false,
        id: null
    }
  }

  handleClose = () => this.setState({show:false});
  handleShow = () => this.setState({show:true});


  render() {
    return (
        <Modal show={this.state.show}  onHide={this.handleClose}>
            <Modal.Header closeButton>
            <Modal.Title>{!!this.props.title && this.props.title}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <form>
                    {
                        !!this.props.inputs && this.props.inputs.map(d => {
                            if(d.type === "text"){
                                return (
                                    <>
                                        <label>{d.label}</label> 
                                        <input className="form-control form-control-sm" 
                                            type={d.type} 
                                            name={d.name}
                                            onChange={this.props.formik.handleChange}
                                            value={this.props.formik.values[d.name]}
                                            disabled={d.disabled}
                                        />
                                    </>
                                )
                            }
                        })
                    }
                    {
                        this.props.inputsExtra
                    }
                </form>
            </Modal.Body>
            <Modal.Footer>
            <Button variant="secondary" onClick={this.handleClose}>
                Cerrar
            </Button>
            {this.props.children}
            <Button variant="primary" onClick={()=>{this.props.actualizar(this.props.formik.values.id)}}>
                Aceptar
            </Button>
            </Modal.Footer>
        </Modal>
    )
  }
}



export default UpdateForm;