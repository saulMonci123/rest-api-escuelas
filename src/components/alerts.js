import { store } from 'react-notifications-component';

export const warning =  (title = "" , message = "", duration = 5000) =>{
    store.addNotification({
        title: title,
        message:message,
        type: "warning",
        insert: "top",
        container: "top-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: duration,
          onScreen: true
        }
    });
}

export const success =  (title = "",message = "", duration = 5000) =>{
    store.addNotification({
        title: title,
        message:message,
        type: "success",
        insert: "top",
        container: "top-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: duration,
          onScreen: true
        }
    });
}

export const danger =  (title = "" , message = "", duration = 5000) =>{
    store.addNotification({
        title: title,
        message:message,
        type: "danger",
        insert: "top",
        container: "top-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: duration,
          onScreen: true
        }
    });
}

export const info =  (title = "", message = "", duration = 5000) =>{
    store.addNotification({
        title: title,
        message:message,
        type: "info",
        insert: "top",
        container: "top-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: duration,
          onScreen: true
        }
    });

}

export const defau =  (title = "" ,message = "", duration = 5000) =>{
    store.addNotification({
        title: title,
        message:message,
        type: "default",
        insert: "top",
        container: "top-right",
        animationIn: ["animate__animated", "animate__fadeIn"],
        animationOut: ["animate__animated", "animate__fadeOut"],
        dismiss: {
          duration: duration,
          onScreen: true
        }
    });
}