import React from "react";

const  FrmBuscarAlumnoColegiatura = ({formik})=> {
    return (
        <div className="container d-flex justify-content-center">
            <div className="card" style={{maxWidth:"100%", margin:"5%"}}>
                <div className="card-header text-left">Buscar Alumno</div>
                <div className="card-body">  
                    <form>
                        <div className="row">
                            <div className="form-group col-xl-6">
                                <input  
                                    onChange={formik.handleChange}
                                    value={formik.values.matricula}
                                    type="text" 
                                    name="matricula"
                                    className="form-control form-control-sm"
                                    placeholder="Matricula"></input>
                            </div>
                            <div className="form-group col-xl-6">
                                <input 
                                    onChange={formik.handleChange}
                                    value={formik.values.nombre}
                                    type="text" 
                                    className="form-control form-control-sm" 
                                    name="nombre"
                                    placeholder="Nombre"></input>
                                    
                            </div>
                        </div>
                        <div className="row">
                            <div className="form-group col-xl-6">
                                <input 
                                    onChange={formik.handleChange}
                                    value={formik.values.apellido_paterno}
                                    type="text" 
                                    className="form-control form-control-sm" 
                                    name="apellido_paterno"
                                    placeholder="Apellido Paterno"></input>
                            </div>
                            <div className="form-group col-xl-6">
                                <input 
                                    onChange={formik.handleChange}
                                    value={formik.values.apellido_materno}
                                    type="text" 
                                    className="form-control form-control-sm" 
                                    name="apellido_materno"
                                    placeholder="Apellido Materno"></input>
                            </div>
                        </div>
                        <div className="form-group">
                            <button type="button" onClick={formik.handleSubmit} className="btn btn-primary btn-block">Aceptar</button>
                        </div> 
                    </form>
                </div>
            </div>
        </div>
    )
}


export default FrmBuscarAlumnoColegiatura;