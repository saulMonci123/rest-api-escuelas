import ReactNotification from 'react-notifications-component';
import 'react-notifications-component/dist/theme.css';
import Routes from "../pages/_routes";

const App = () => {
    return (
      <div className="app-container">
        <ReactNotification />
        <Routes />
      </div>
    )
  };

export default App;  