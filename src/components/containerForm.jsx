import React from 'react';



class ContainerForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() { 
        return ( 
            <div className="container d-flex justify-content-center">
                <div className="card" style={{marginTop:"10%", width:"40%"}}>
                    <div class="card-header text-left">{this.props.titulo}</div>
                    <div className="card-body">  
                        <form>
                            {
                                this.props.children
                            } 
                        </form>
                    </div>
                </div>
            </div>
         );
    }
}
 
export default ContainerForm;