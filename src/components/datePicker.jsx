import React, { useState } from "react";
import DatePicker from "react-datepicker";
 
import "react-datepicker/dist/react-datepicker.css";
 
// CSS Modules, react-datepicker-cssmodules.css
import 'react-datepicker/dist/react-datepicker-cssmodules.css';
 
const DatePickerComponent = ({placeholder, onChange}) => {
  const [startDate, setStartDate] = useState(null);
  return (
    <DatePicker 
        selected={!!startDate && startDate}
        onChange={date =>{ 
            setStartDate(date);
            onChange(date)
        }} 
        className="form-control form-control-sm" 
        placeholderText={placeholder}
        dateFormat="dd/MM/yyyy"        
    />
  );
};

export default DatePickerComponent;