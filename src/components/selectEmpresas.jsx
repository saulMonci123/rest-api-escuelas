import React, { Component, useEffect, useState } from 'react';
import { empresasServices } from "../services/Empresas/empresas.service";

//Se instancea el servicio de empresas
const empresasSrv = new empresasServices;

function  SelectEmpresas(props) {
   const [data, setData] = useState([]);

   const getEmpresas = async () => {
      const data = await empresasSrv.getJson();
      setData([...data]);
   }

   useEffect(()=>{
      getEmpresas();
   },[]);

   return (
      <>
         {
            
               props.label === undefined && <label>Seleccione una empresa</label>

         }
         <select
            className="form-control form-control-sm"
            onChange={props.formik.handleChange}
            value={props.formik.values['empresa']}
            name="empresa"
            disabled={true}
         >
            {
               !!data && data.map((d) => {
                  return (
                     <option id={d.id} value={d.id}  >{d.nombre}</option>
                  )
               })
            }
         </select>
      </>
   );
}

export default SelectEmpresas;