import React, {Component} from 'react';


class CrudView extends Component {

    constructor(props) {
        super(props);
    }
    render() { 
        const {title, buttons, table} = this.props;
        return ( 
            <div className="card" style={{ marginTop:"5%"}}>
                <div className="card-header">
                    <div className="row">
                        <div className="col text-left"><h2>{title}</h2></div>
                        <div className="col text-right">
                            {buttons}
                        </div>
                    </div>
                </div>
                <div className="card-body text-center">
                    {table}
                </div>
            </div>
         );

    }
}
 
export default CrudView;