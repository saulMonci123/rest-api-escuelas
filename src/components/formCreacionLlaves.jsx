import React, { useEffect, useState, useRef } from 'react';
import SelectEmpresas from './selectEmpresas';
import { habitacionesServices } from '../services/habitaciones/habitaciones.service';
import { useEmpresaContext } from '../context/EmpresaContext';
import Select from 'react-select';
import { AreasComunesService } from '../services/areasComunes/areasComunes.service';
import DatePickerComponent from './datePicker';
import ClientsSelect from './clientsSelect';
function FormCreacionLlaves(props) {
    const {formik}  = props;
    const empresa = useEmpresaContext();
    const [options, setOptions] = useState([]);  
    const [optionsAreasComun, setoptionsAreasComun] = useState([]);
    const [disabledMulti, setDisabledMulti] = useState(true);
    useEffect(()=>{
        getHabitacionesJson();
        getAreasComunJson();
    },[empresa, disabledMulti]);

    const toggleDisabled = () => {
        setDisabledMulti(!disabledMulti)
    }

    const getHabitacionesJson = async () => {
        const habSrv = new habitacionesServices;
        const data = await habSrv.getHabitacionesJson(empresa);
        let array = [];
        data.map((d)=>{
            array.push({
                value:d.id,
                label:d.nombre
            }); 
        })
        setOptions([...array]);
    }


    const getAreasComunJson =  async () => {
        const areasComunSrv = new AreasComunesService;
        const data = await areasComunSrv.getAreasComunesJson(empresa);
        let array = [];
        data.map((d)=>{
            array.push({
                value:d.id,
                label:d.nombre
            }); 
        })
        setoptionsAreasComun([...array]);
    }

    const styles = {
        width:"100%"
    }

    const formatterHour = (date)=>{
        const newDate = new Date(date);
        const dd = String(newDate.getDate()).padStart(2, "0");
        const mm = String(newDate.getMonth() + 1).padStart(2, "0"); //January is 0!
        const yyyy = newDate.getFullYear();
        return yyyy + "-" + mm + "-" + dd;
    }

    return (    
        <>
            <div className="form-group">
                <ClientsSelect
                    formik={formik}
                />
            </div><div className="form-group">
                <SelectEmpresas
                    label={false}
                    formik={formik}
                />
            </div>
            <div className="form-group form-row">
                <div className="col">
                    <input type="text" className="form-control form-control-sm" name="nombre" onChange={formik.handleChange} placeholder="Nombre"></input>
                </div>
            </div>
            <div className="form-group form-row">
                <div className="col text-left">
                        <Select
                            name="colors"
                            options={options}
                            className="basic-multi-select form-group-sm"
                            classNamePrefix="select"
                            isDisabled={false}
                            placeholder="Seleccione habitacion principal"
                            onChange={(data)=>{
                                formik.setFieldValue('habitacion_principal', data)
                            }}
                        />
                </div>
            </div> 
            <div className="form-group form-row">
                
                <div className="col-2">
                    <div class="input-group-text" style={{display:"block"}}>
                        <input type="checkbox" onChange={toggleDisabled} aria-label="Checkbox for following text input"></input>
                    </div>
                </div>
                <div className="col-10 text-left">
                        <Select
                            isMulti
                            name="colors"
                            options={options}
                            className="basic-multi-select form-group-sm"
                            classNamePrefix="select"
                            isDisabled={disabledMulti}
                            placeholder="Seleccione las habitaciones"
                            onChange={(data)=>{
                                formik.setFieldValue('habitaciones', data)
                            }}
                        />
                </div>
            </div>
            <div className="form-group form-row">
                <div className="col text-left">
                    <Select
                        isMulti
                        name="colors"
                        options={optionsAreasComun}
                        className="basic-multi-select form-group-sm"
                        classNamePrefix="select"
                        isDisabled={false}
                        placeholder="Seleccione las areas comun"
                        onChange={(data)=>{
                            formik.setFieldValue('areas', data)
                        }}
                    />
                </div>
            </div>
            <div className="form-row text-left">
                <div className="col-6">
                    <label>Fecha y hora de entrada</label>
                </div>
            </div>
            <div className="form-group form-row">
                <div className="col-4">
                    <DatePickerComponent
                        placeholder="Fecha llegada"
                        onChange={(data)=>{
                            formik.setFieldValue('fecha_llegada', formatterHour(data))
                        }}
                    />
                </div>
                <div className="col-4">
                    <input name="hora_entrada" onChange={formik.handleChange} className="form-control form-control-sm" type="time"></input>
                </div>
            </div> 
            <div className="form-row text-left">
                <div className="col-6">
                    <label>Fecha y hora de salida</label>
                </div>
            </div>
            <div className="form-group form-row">
                <div className="col-4">
                    <DatePickerComponent
                        placeholder="Fecha salida"
                        onChange={(data)=>{
                            formik.setFieldValue('fecha_salida', formatterHour(data))
                        }}
                    />
                </div>
                <div className="col-4">
                    <input name="hora_salida" onChange={formik.handleChange} className="form-control form-control-sm" type="time"></input>
                </div>
            </div>
            <div className="form-group form-row">
                <div className="col-4">
                    <input type="number" name="cantidad" onChange={formik.handleChange} className="form-control form-control-sm" placeholder="Cantidad de llaves"></input>
                </div>
            </div>
           
            <button type="button" onClick={formik.handleSubmit} className="btn btn-primary btn-sm btn-block">Aceptar</button>
        </> 
    );
}
 
export default FormCreacionLlaves;