import {Http} from "../Auth/http"
import {url} from "../../enviroments";


export class AreasComunesService{
    async getAreasComunes(page, empresa){
        return await Http.get(`${url}/api/areas/comunes?page=${page}&empresaId=${empresa}`);
    }


    async create(data){
        return await Http.post(`${url}/api/areas/comunes/create/`, data);
    }

    async delete(id){
        return await Http.delete(`${url}/api/areas/comunes/delete?id=${id}`);
    }

    async get(id){
        return await Http.get(`${url}/api/areas/comunes/find?id=${id}`);
    }
    
    async update(id,values){
        return await Http.put(`${url}/api/areas/comunes/update?id=${id}`,values)
    }

    async getAreasComunesJson(empresa){
        return await Http.get(`${url}/api/areas/comunes/json?empresaId=${empresa}`);
    }

}