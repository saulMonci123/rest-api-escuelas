import axios from "axios";
import { AuthHelperService } from "./auth.service"
const authSrv = new AuthHelperService;

//Configuraciones para peticiones http con axios
const request = axios.create();
authSrv.initalAxiosInterceptor(request);
export const Http = request;
