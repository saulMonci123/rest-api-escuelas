import {url} from "../../enviroments";
import axios  from "axios";
import { store } from 'react-notifications-component';
import { danger } from "../../components/alerts";



export class AuthHelperService {

    constructor(){
    }

    async setToken(token){
        await localStorage.setItem('TOKEN', token);
    }

    getToken(){
        return localStorage.getItem('TOKEN')
    }

    removeToken(){
        localStorage.removeItem('TOKEN');
    }

    async setUser(user){
        await localStorage.setItem('USER', user );
    }

    getUser(){
        return JSON.parse(localStorage.getItem('USER'));
    }

    removeUser(){
        localStorage.removeItem('USER');
    }
    

    initalAxiosInterceptor(request){
        request.interceptors.request.use(async(config)=>{
            const token = await localStorage.getItem('TOKEN');
            if(token !== "undefined" && token !== null){
                config.headers['auth-token'] = token;
            }
            return config;
        });
        request.interceptors.response.use((response)=>{
            return response.data;
        },(error)=>{
            if(error.response.data.name === "TokenExpiredError"){
                this.removeToken();
                this.removeUser();
                window.location.href = "/login?expire=true";
            }

            if(error.response.status === 400){
                danger("Error", error.response.data)
            }
        });
        return Promise.reject(request.error);
    }

    async login(data){
        return await axios.post(`${url}/api/auth/signin`,data);
    }

}