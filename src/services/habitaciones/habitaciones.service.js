import {Http} from "../Auth/http"
import {url} from "../../enviroments";

export class habitacionesServices {

    async getHabitaciones(page, empresa){
        return await Http.get(`${url}/api/rooms?page=${page}&empresaId=${empresa}`);
    }


    async create(data){
        return await Http.post(`${url}/api/rooms/create/`, data);
    }

    async delete(id){
        return await Http.delete(`${url}/api/rooms/delete?id=${id}`);
    }

    async get(id){
        return await Http.get(`${url}/api/rooms/find?id=${id}`);
    }
    
    async update(id,values){
        return await Http.put(`${url}/api/rooms/update?id=${id}`,values)
    }

    async createTipoHab(data){
        return await Http.post(`${url}/api/rooms/create/tipoHabitacion`, data);
    }


    async getTipoHabitaciones(page, empresa){
        return await Http.get(`${url}/api/rooms/types?page=${page}&empresaId=${empresa}`);
    }


    async deleteTipoHabitacion(id){
        return await Http.delete(`${url}/api/rooms/delete/tipoHabitacion?id=${id}`);
    }
    
    async getTipoHabitacion(id){
        return await Http.get(`${url}/api/rooms/find/tipoHabitacion?id=${id}`);
    }

    async updateTipoHabitacion(id, values){
        return await Http.put(`${url}/api/rooms/update/tipoHabitacion?id=${id}`, values);
    }

    async getTipoHabitacionJson(id){
        return await Http.get(`${url}/api/rooms/json/tipoHabitacion?id=${id}`);
    }

    async getTiposHabitacionByEmpresa(id){
        return await Http.get(`${url}/api/rooms/json/tipoHabitacion/empresa?id=${id}`);
    }

    async getHabitacionesJson(empresa){
        return await Http.get(`${url}/api/rooms/json?empresaId=${empresa}`);
    }
}