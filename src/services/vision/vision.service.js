import {Http} from "../Auth/http"
import {url} from "../../enviroments";

export default class visionServices {

    async crearLlave(data){
        return await Http.post(`${url}/api/vincard/crear/llave`, data);
    }
    
    async createCheckIn(data){
        return await Http.post(`${url}/api/vincard/check-in`, data);
    }
}