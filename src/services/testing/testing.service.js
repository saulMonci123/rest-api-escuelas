import {Http} from "../Auth/http"
import {url} from "../../enviroments";

export class TestingServices {

    async getClients(){
        return await Http.get(`${url}/api/sockets/socket`);
    }

    async dispararAccion(data){
        return await Http.post(`${url}/api/llave`,data);
    }
}