import {Http} from "../Auth/http"
import {url} from "../../enviroments";

export class empresasServices {

    async getHabitaciones(page){
        return await Http.get(`${url}/api/company?page=${page}`);
    }


    async create(data){
        return await Http.post(`${url}/api/company/create/`, data);
    }

    async delete(id){
        return await Http.delete(`${url}/api/company/delete?id=${id}`);
    }

    async get(id){
        return await Http.get(`${url}/api/company/find?id=${id}`);
    }
    
    async update(id,values){
        return await Http.put(`${url}/api/company/update?id=${id}`,values)
    }


    async createTipoHab(data){
        return await Http.post(`${url}/api/company/create/tipoHabitacion`, data);
    }


    async getTipoHabitaciones(page){
        return await Http.get(`${url}/api/company/types?page=${page}`);
    }


    async deleteTipoHabitacion(id){
        return await Http.delete(`${url}/api/company/delete/tipoHabitacion?id=${id}`);
    }

    async getJson (){
        return await Http.get(`${url}/api/company/json`);
    }
}