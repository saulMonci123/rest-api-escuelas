// import axios from "axios";
import {Http} from "../Auth/http"
import {url} from "../../enviroments";


export class estacionesServices{

    async getEstaciones(page, empresa){
        return await Http.get(`${url}/api/clients?page=${page}&empresaId=${empresa}`);
    }

    async create(data){
        return await Http.post(`${url}/api/clients/create`, data);
    }

    async delete(id){
        return await Http.delete(`${url}/api/clients/delete?id=${id}`);
    }

    async get(id){
        return await Http.get(`${url}/api/clients/find?id=${id}`);
    }

    async update(id,values){
        return await Http.put(`${url}/api/clients/update?id=${id}`,values);
    }

}








