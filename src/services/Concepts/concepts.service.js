import {Http} from "../Auth/http"
import {url} from "../../enviroments";

export class conceptsServices {

    async getConcepts(){
        return await Http.get(`${url}/api/concepts/`);
    }


    async getTiposPagos(){
        return await Http.get(`${url}/api/concepts/getTypePays`)
    }

    async realizarPago(data){
        return await Http.post(`${url}/api/concepts/makePay`, data)
    }
}